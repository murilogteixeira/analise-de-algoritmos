//ALGORITMO GUSTAVO

#include <stdio.h>
#include <stdlib.h>

// Peso maximo 10
#define PESO_MAX 10

int encherMochila(int pesos[], int valores[]);
int maior(int a, int b);


int main(int argc, char *argv[]) {
    int pesos[] = {7, 3, 4, 5};
    int valores[] = {42, 12, 40, 25};
    int maiorValor = encherMochila(pesos, valores);    
    printf("Maior valor obtido: %d", maiorValor);
}


int encherMochila(int pesos[], int valores[]){
    int values[5][PESO_MAX + 1];//1
    int i = 0, j = 0;//1
    
    for(i = 0; i <= 4; i++){//n+1
        for(j = 0; j <= 10; j++){//n*n+1
            if(i == 0 || j == 0){//n*n
                values[i][j] = 0;
            }else if (pesos[i-1] <= j){//n*n
                values[i][j] = maior( (valores[i-1] + values[i-1][j-pesos[i-1]]), values[i-1][j]);
            }else{//n*n
                values[i][j] = values[i-1][j];//n*n
            }
        }
    }
    return values[4][10];//1
}//1+1+n+1+n²+1+n²+n²+n²+n²++1 => 5n²+n+5

int maior(int a, int b) { 
    if(a > b){
        return a;
    }else{
        return b;
    }
} 
