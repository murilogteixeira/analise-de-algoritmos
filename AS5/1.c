#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define QTD_CHAR 20

int strMatch(char str[], char chave[]){
    int i, j = 0, k, tamStr = strlen(str), tamChave = strlen(chave);
    
    for (i = 0; i < tamStr; i++) {
        k = i;
        
        if(str[i] == chave[j]){
            printf("%c[%d] == %c[%d]\n", str[i], i, chave[j], j);
            j++;
        }else{
            if(j > 0){
                j = 0;
                i = k+1;
            }
        }
        
        if(j == tamChave){
            return (i - j) + 1;
        }else{
            int charRest = tamStr+1 - i;
            if(charRest < tamChave){
                return -1;
            }
        }
    }
}

int main(){
    
    int pos;
    char str[QTD_CHAR], chave[QTD_CHAR];

    printf("Insira um texto (máx %d caracteres): ", QTD_CHAR);
    __fpurge(stdin);
    scanf("%[^\n]s", str);
    
    printf("Insira uma chave: ");
    __fpurge(stdin);
    scanf("%[^\n]s", chave);
    
    pos = strMatch(str, chave);
    
    if(pos < 0){
        printf("String nao encontrada.\n");
        return 0;
    }

    printf("Substring encontrada na posicao %d da string.\n", pos);
    return 0;
}

