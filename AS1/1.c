/*
 1.Receba um número e imprima os n primeiros termos da série de Fibonacci;
*/

#include <stdio.h>
#include <stdlib.h>

void fibo(int);

int main(){
    int num = 0;
    printf("Insira o limite para a sequencia de fibonacci (minimo 2): ");
    scanf("%d", &num);
    fibo(num);
    printf("\n");
}

void fibo(int num){
    int i, ant = 0, atual = 1, prox = 0;
    
    printf("%d, %d", ant, atual);
    
    for(i = 2; i < num; i++){
        prox = ant + atual;
        ant = atual;
        atual = prox;
        printf(", %d", atual);
    }
}