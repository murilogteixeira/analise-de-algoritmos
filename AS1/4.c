/*
 4.Otimize o terceiro algoritmo da apresentação (pesquisa sequencial);
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int pesquisaVet(int chave, int tam, int vet[]);

int main(){
    int tam = 10;
    int vet[tam];
    int i;

    srand(time(NULL));

    for (i = 0; i < tam; i++){
        int j;

        vet[i] = rand() % 100;
        
        for(j = 0; j < i; j++){
            if(vet[j] == vet[i]){
                i--;
            }
        }
    }

    for(i = 0; i < 10; i++){
        printf("%d  ", vet[i]);
    }

    printf("\n");

    sleep(3);
    system("clear");

    int chave, res;

    printf("Insira a chave para busca: ");
    scanf("%d", &chave);

    res = pesquisaVet(chave, tam, vet);

    for(i = 0; i < 10; i++){
        printf("%d  ", vet[i]);
    }

    if(res < 0){
        printf("\nChave nao encontrada.\n");
        return 0;
    }
    printf("\nChave encontrada na posicao %d do vetor.\n", res+1);
}

int pesquisaVet(int chave, int tam, int vet[]){
    int i;
    for(i = 0; i < tam; i++){
        if(vet[i] == chave){
            return i;
        }
    }
    return -1;
}