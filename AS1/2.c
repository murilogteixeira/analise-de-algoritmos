/*
 2.Receba dois números e calcule o primeiro elevado ao segundo;
*/

#include <stdio.h>
#include <stdlib.h>

int calc(int, int);

int main(){
    int num1 = 0, num2 = 0, res = 0;
    
    printf("Insira a base:\t\t");
    scanf("%d", &num1);
    printf("Insira o expoente:\t");
    scanf("%d", &num2);
    
    res = calc(num1, num2);
    
    printf("O resultado de %d ^ %d é %d", num1, num2, res);
}

int calc(int num1, int num2){
    int i = 1, n = num1;

    if(num2 == 0){
        return 1;
    }else{
        if(num2 == 1){
            return num1;
        }
    }
    
    for(i = 1; i < num2; i++){
        n *= num1;
    }
    
    return n;
}