/*
 3.Encontre o maior elemento de uma matriz quadrada;
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int verificaMaior(int tam, int[tam][tam]);

int main(){
    int tam;
    printf("Digite o tamanho da matriz quadrada: ");
    scanf("%d", &tam);
    int mat[tam][tam];
    int i, j, maior;

    srand(time(NULL));

    for (i = 0; i < tam; i++){
        for(j = 0; j < tam; j++){
            mat[i][j] = rand() % 1000;
        }
    }

    maior = verificaMaior(tam, mat);

    printf("O maior número da matriz é %d.\n", maior);
}

int verificaMaior(int tam, int mat[tam][tam]){
    int maior = 0, i, j;
    for (i = 0; i < tam; i++){
        for(j = 0; j < tam; j++){
            if(mat[i][j] > maior){
                maior = mat[i][j];
            }
        }
    }
    return maior;
}